import { Weather } from './../model/weather';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { catchError } from 'rxjs/operators';


@Injectable()
export class WeatherService {
  url = 'https://api.openweathermap.org/data/2.5/forecast';

  constructor(private http: HttpClient) { }

  buildSearchParams(cityName: string): string {
    const params = {
      q: cityName,
      cnt: '8',
      units: 'metric',
      APPID: '010721642521f31b0fbc8c3831d45951'
    };
    return Object.keys(params).map(key => key + '=' + params[key]).join('&');
  }

  searchByCity(cityName: string): Observable<Weather> {
    const searchParams = this.buildSearchParams(cityName);
    const url = `${this.url}?${searchParams}`;
    return this.http.get(url);
  }

}

import { of } from 'rxjs/observable/of';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { hot, cold } from 'jasmine-marbles';
import { Observable } from 'rxjs/Observable';

import { StoreModule } from '@ngrx/store';
import { Actions } from '@ngrx/effects';
import * as fromWeatherEffects from './weather.effects';
import * as fromWeatherActions from './weather.actions';
import { WeatherService } from './../weather.service';
import { expectedWeatherContentState } from './../api-samples/expected-state';


class TestActions extends Actions {
  constructor() {
    super(of());
  }

  set stream(source: Observable<any>) {
    this.source = source;
  }
}

function getActions() {
  return new TestActions();
}

describe('WeatherEffects', () => {
  let actions$: TestActions;
  let provider: WeatherService;
  let effects: fromWeatherEffects.WeatherEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        WeatherService,
        fromWeatherEffects.WeatherEffects,
        { provide: Actions, useFactory: getActions }
      ]
    });

    actions$ = TestBed.get(Actions);
    provider = TestBed.get(WeatherService);
    effects = TestBed.get(fromWeatherEffects.WeatherEffects);

    spyOn(provider, 'searchByCity').and.returnValue(of(expectedWeatherContentState[0]));

  });

  /* Couldn't figure out what's wrong with this test */
  xdescribe('searchByCity$', () => {
    it('should return an object from SearchWeatherByCitySuccess', () => {
      const action = new fromWeatherActions.SearchWeatherByCity('abc');
      const completion = new fromWeatherActions.SearchWeatherByCitySuccess(
        expectedWeatherContentState[0]
      );

      actions$.stream = hot('a', { a: action });
      const expected = hot('b', { b: completion });

      expect(effects.searchByCity$).toBeObservable(expected);
    });
  });

});

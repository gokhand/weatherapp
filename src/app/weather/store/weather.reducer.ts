import * as fromWeatherState from './weather.state';
import * as fromWeatherActions from './weather.actions';

export function WeatherReducer (
  state = fromWeatherState.initialState,
  action: fromWeatherActions.WeatherActions
): fromWeatherState.WeatherState {
  switch (action.type) {
    case fromWeatherActions.SEARCH_WEATHER_BY_CITY: {
      return {
        ...state,
        loading: fromWeatherState.LoadingState.Pending
      };
    }
    /*
      - I'd probably check for duplicates before appending the new into the array
      - This would require city lookup that returns some sort of unique id
      - I'd use the unique id to check if any of the cities in the array has this id, in which case do not update the array
    */
    case fromWeatherActions.SEARCH_WEATHER_BY_CITY_SUCCESS: {
      return {
        ...state,
        content: [
          ...state.content,
          action.payload
        ],
        loading: fromWeatherState.LoadingState.Success
      };
    }

    case fromWeatherActions.SEARCH_WEATHER_BY_CITY_FAIL: {
      return {
        ...state,
        loading: fromWeatherState.LoadingState.Fail
      };
    }

  }

  return state;
}

import { createSelector } from '@ngrx/store';
import { createFeatureSelector } from '@ngrx/store';
import { WeatherState } from './weather.state';

export const getWeatherState = createFeatureSelector<WeatherState>('weather');

export const getWeatherContent = createSelector(
  getWeatherState,
  (state: WeatherState) => state.content
);

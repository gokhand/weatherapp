import { Weather } from './../../model/weather';

export enum LoadingState {
  NotSent = 'Not sent',
  Pending = 'Pending',
  Success = 'Success',
  Fail = 'Fail'
}

export interface WeatherState {
  content: Weather[];
  loading: LoadingState;
}

export const initialState: WeatherState = {
  content: [],
  loading: LoadingState.NotSent
};

export function GetInitialState(): WeatherState {
  return initialState;
}

import { expectedWeatherContentState } from './../api-samples/expected-state';
import * as fromWeatherReducer from './weather.reducer';
import * as fromWeatherActions from './weather.actions';
import * as fromWeatherState from './weather.state';

describe('Weather Reducer', () => {
  describe('SEARCH_WEATHER_BY_CITY actions', () => {
    describe('SEARCH_WEATHER_BY_CITY_ITEMS action', () => {
      it('should set loading state to pending', () => {
        const { initialState } = fromWeatherState;
        const action = new fromWeatherActions.SearchWeatherByCity('abc');
        const state = fromWeatherReducer.WeatherReducer(initialState, action);
        const expectedState = {
          ...state,
          loading: fromWeatherState.LoadingState.Pending
        };
        expect(state).toEqual(expectedState);
      });
    });

    describe('SEARCH_WEATHER_BY_CITY_ITEMS_SUCCESS action', () => {
      it('should populate the weather array', () => {
        const { initialState } = fromWeatherState;
        const action = new fromWeatherActions.SearchWeatherByCitySuccess(
            expectedWeatherContentState[0]
        );
        const state = fromWeatherReducer.WeatherReducer(initialState, action);
        const expectedResponse: fromWeatherState.WeatherState = {
          content: expectedWeatherContentState,
          loading: fromWeatherState.LoadingState.Success
        };
        expect(state.loading).toEqual(expectedResponse.loading);
        expect(state.content).toEqual(expectedResponse.content);
      });
    });

    describe('SEARCH_WEATHER_BY_CITY_ITEMS_FAIL action', () => {
      it('should set the loading state to Error', () => {
        const { initialState } = fromWeatherState;
        const action = new fromWeatherActions.SearchWeatherByCityFail();
        const state = fromWeatherReducer.WeatherReducer(initialState, action);
        expect(state.loading).toEqual(fromWeatherState.LoadingState.Fail);
      });
    });
  });

});

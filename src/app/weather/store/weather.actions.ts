import { Weather } from './../../model/weather';
import { Action } from '@ngrx/store';

export const SEARCH_WEATHER_BY_CITY = '[Search weather] Search city weather';
export const SEARCH_WEATHER_BY_CITY_FAIL = '[Search weather] Search city weather fail';
export const SEARCH_WEATHER_BY_CITY_SUCCESS = '[Search weather] Search city weather success';

export class SearchWeatherByCity implements Action {
  readonly type = SEARCH_WEATHER_BY_CITY;
  constructor(public payload: string) {}
}

export class SearchWeatherByCityFail implements Action {
  readonly type = SEARCH_WEATHER_BY_CITY_FAIL;
}

export class SearchWeatherByCitySuccess implements Action {
  readonly type = SEARCH_WEATHER_BY_CITY_SUCCESS;
  constructor(public payload: Weather) {}
}

export type WeatherActions =
  | SearchWeatherByCity
  | SearchWeatherByCityFail
  | SearchWeatherByCitySuccess;

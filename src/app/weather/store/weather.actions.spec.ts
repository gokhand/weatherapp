import * as fromWeatherActions from './weather.actions';

import {
  expectedWeatherContentState,
} from '../api-samples/expected-state';

describe('Weather Actions', () => {
  describe('SearchWeatherByCity', () => {
    it('should create an action', () => {
      const action = new fromWeatherActions.SearchWeatherByCity('abc');

      expect({ ...action }).toEqual({
        type: fromWeatherActions.SEARCH_WEATHER_BY_CITY,
        payload: 'abc'
      });
    });
  });

  describe('SearchWeatherByCityFail', () => {
    it('should create an action', () => {
      const action = new fromWeatherActions.SearchWeatherByCityFail();

      expect({ ...action }).toEqual({
        type: fromWeatherActions.SEARCH_WEATHER_BY_CITY_FAIL
      });
    });
  });

  describe('SearchWeatherByCitySuccess', () => {
    it('should create an action', () => {
      const action = new fromWeatherActions.SearchWeatherByCitySuccess(expectedWeatherContentState[0]);
      const expectedResponse = expectedWeatherContentState[0];
      expect({ ...action }).toEqual({
        type: fromWeatherActions.SEARCH_WEATHER_BY_CITY_SUCCESS,
        payload: expectedResponse
      });
    });
  });
});



import { StoreModule, Store } from '@ngrx/store';

import { TestBed } from '@angular/core/testing';
import * as fromWeatherState from './weather.state';
import * as fromWeatherReducer from './weather.reducer';
import * as fromWeatherActions from './weather.actions';
import * as fromWeatherSelectors from './weather.selectors';
import { expectedWeatherContentState } from './../api-samples/expected-state';

describe('Weather Selectors', () => {
  let store: Store<fromWeatherState.WeatherState>;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot({
          weather: fromWeatherReducer.WeatherReducer
        })
      ]
    });

    store = TestBed.get(Store);
  });

  describe('getWeatherState Feature state', () => {
    it('should return state of weather store', () => {
      const { initialState } = fromWeatherState;
      let result;
      /* tslint:disable-next-line:ban */
      store.select(fromWeatherSelectors.getWeatherState).subscribe(value => (result = value));

      expect(result).toEqual(initialState);
    });
  });

  describe('getWeatherContent', () => {
    it('should return the weather content as an object', () => {
      let result;

      /* tslint:disable-next-line:ban */
      store.select(fromWeatherSelectors.getWeatherContent).subscribe(value => (result = value));

      expect(result).toEqual([]);
      store.dispatch(
        new fromWeatherActions.SearchWeatherByCitySuccess(expectedWeatherContentState[0])
      );
      expect(result).toEqual([expectedWeatherContentState[0]]);
    });
  });

});

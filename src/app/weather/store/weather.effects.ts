import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import * as WeatherActions from './weather.actions';
import { WeatherService } from '../weather.service';
import { map, mergeMap, catchError, debounceTime } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';

@Injectable()
export class WeatherEffects {
  constructor(
    private actions$: Actions,
    private weatherService: WeatherService
  ) {}

  @Effect()
  searchByCity$ = this.actions$.pipe(
    ofType<WeatherActions.SearchWeatherByCity>(WeatherActions.SEARCH_WEATHER_BY_CITY),
    debounceTime(500),
    mergeMap((action) => this.weatherService.searchByCity(action.payload)
      .pipe(
        map(weather => ({ type: WeatherActions.SEARCH_WEATHER_BY_CITY_SUCCESS, payload: weather})),
        catchError(() => of({ type: WeatherActions.SEARCH_WEATHER_BY_CITY_FAIL }))
      )
    )
  );

}


import { Observable } from 'rxjs/Observable';
import { Http, Response, ResponseOptions } from '@angular/http';
import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs/observable/of';
import { WeatherService } from './weather.service';

import 'rxjs/add/observable/of';

function createResponse(body) {
  return Observable.of(
    new Response(new ResponseOptions({ body: JSON.stringify(body) }))
  );
}

class HttpServiceMock {
  get() {
    return createResponse([]);
  }
}


describe('WeatherService', () => {
  let provider: WeatherService;
  let http: Http;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [WeatherService, { provide: Http, useClass: HttpServiceMock }]
    });
    http = TestBed.get(Http);
    provider = TestBed.get(WeatherService);
  });

  it('should return the weather json', () => {
    spyOn(provider, 'searchByCity').and.returnValue(of({}));

    /* tslint:disable-next-line:ban */
    provider.searchByCity('abc').subscribe(result => {
      expect(result).toEqual({});
    });
  });
});

import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html'
})
export class SearchComponent {
  cityName: string;

  @Output()
  searched: EventEmitter<string> = new EventEmitter();

  constructor() { }

  search() {
    this.searched.emit(this.cityName);
    this.cityName = '';
  }
}

import { Weather } from '../../model/weather';

export const expectedWeatherContentState: Weather[] = [
  {
    city: {
      coord: {lat: 40.7306, lon: -73.9867},
      country: 'US',
      id: 5128581,
      name: 'New York',
      population: 8175133
    },
    cod: '200',
    message: 0.0114,
    list: [
      {
        dt: 1557954000,
        main: {
          grnd_level: 1002.43,
          humidity: 37,
          pressure: 1009.52,
          sea_level: 1009.52,
          temp: 20.94,
          temp_kf: 0.89,
          temp_max: 20.94,
          temp_min: 20.05
        },
        weather: [
          {
            description: 'broken clouds',
            icon: '04d',
            id: 803,
            main: 'Clouds'
          }
        ],
        clouds: { all: 57 },
        wind: { deg: 306.823, speed: 2.96 },
        sys: { pod: 'd' },
        dt_txt: '2019-05-15 21:00:00',
      }
    ]
  }
];

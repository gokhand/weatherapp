import { Observable } from 'rxjs/Observable';
import { Weather } from './../model/weather';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromStore from './store';


@Component({
  selector: 'app-weather',
  template: `
    <app-search (searched)="onCitySearch($event)"></app-search>
    <app-results [weatherContent]="weatherContent$ | async"></app-results>
  `
})
export class WeatherContainerComponent implements OnInit {

  weatherContent$: Observable<Weather[]>;

  constructor(public store: Store<fromStore.WeatherState>) {}

  ngOnInit() {
    this.weatherContent$ = this.store.select(fromStore.getWeatherContent);
  }

  onCitySearch(cityName: string) {
    this.store.dispatch(new fromStore.SearchWeatherByCity(cityName));
  }
}

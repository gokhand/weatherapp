import { WeatherService } from './weather.service';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Actions } from '@ngrx/effects';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { WeatherContainerComponent } from './weather.container';
import { HttpClientModule } from '@angular/common/http';

import { CommonModule } from '@angular/common';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { WeatherReducer, WeatherEffects, GetInitialState, SearchWeatherByCity } from './store';

describe('WeatherContainer', () => {
  let component: WeatherContainerComponent;
  let fixture: ComponentFixture<WeatherContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeatherContainerComponent ],
      providers: [Actions, WeatherService],
      imports: [
        HttpClientModule,
        CommonModule,
        StoreModule.forRoot({}),
        EffectsModule.forRoot([]),
        StoreModule.forFeature('weather', WeatherReducer, {
          initialState: GetInitialState
        }),
        EffectsModule.forFeature([WeatherEffects])
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should dispatch action when onCitySearch is called', () => {
    const cityName = 'paris';
    spyOn(component.store, 'dispatch');
    component.onCitySearch(cityName);
    expect(component.store.dispatch).toHaveBeenCalledWith(new SearchWeatherByCity(cityName));
  });

});

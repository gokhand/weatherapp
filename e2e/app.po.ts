import { browser, by, element } from 'protractor';

export class AppPage {

  navigateTo() {
    return browser.get('/');
  }

  populateSearch(cityName) {
    const searchInput = element(by.id('city_search_input'));
    return searchInput.sendKeys(cityName);
  }

  clickSubmit() {
    const submitButton = element(by.id('city_search_submit'));
    return submitButton.click();
  }

  getSearchResultRowsCityName() {
    return element.all(by.className('weather-search-results')).first().all(by.tagName('td')).first().getText();
  }

  getSearchResultRowCount() {
    return element.all(by.className('weather-search-results')).count();
  }

  getParagraphText() {
    return element(by.css('app-root h1')).getText();
  }
}

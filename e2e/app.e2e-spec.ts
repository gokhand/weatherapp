import { AppPage } from './app.po';
import { browser, by, element } from 'protractor';

describe('angular-weather App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
    page.navigateTo();
  });

  it('e2e are running empty, please implement', () => {
    expect(page);
  });

  it('should populate the page with some results when search and submit happens', () => {
    const cityName = 'London';
    page.populateSearch(cityName).then(() => {
      page.clickSubmit();
      browser.sleep(3000);
      page.getSearchResultRowsCityName().then( result => {
        expect(result).toContain(cityName);
      });
    });
  });

  it('should add new cities into new rows', () => {
    page.populateSearch('London').then(() => {
      page.clickSubmit();
      browser.sleep(3000);

      page.populateSearch('Paris').then(() => {
        page.clickSubmit();
        browser.sleep(3000);

        page.getSearchResultRowCount().then( result => {
          expect(result).toEqual(2);
        });
      });

    });
  });
});
